/**
 * Created by James on 9/9/2017.
 */
// this file must be .js for loading schema from .graphql file
import schema from './lib/schema.graphql';
export default schema;