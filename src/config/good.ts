/**
 * Created by jhenley on 2/14/2017.
 */
export const good_options = {
  ops: {interval: 30000},
  reporters: {
    your_console_reporter: [{
      module: 'good-squeeze',
      name: 'Squeeze',
      args: [{log: '*', error: '*', request: '*', response: '*', ops: '*'}]
    }, {
      module: 'good-console'
    },
      'stdout']
  }
};
export default good_options;