import {makeExecutableSchema} from "graphql-tools";
import * as path from 'path';
import * as deepmerge from 'deepmerge';
import schema from "./schema";
import {fileLoader} from 'merge-graphql-schemas';

const resolversArray = fileLoader(path.join(__dirname, './resolvers'));
export const resolvers = deepmerge.all([{}, ...resolversArray]).resolver;

export const executableSchema = makeExecutableSchema({
    typeDefs: [schema],
    resolvers: resolvers
});