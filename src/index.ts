import * as Hapi from "hapi";
import {plugins} from './plugins';
global.XMLHttpRequest = require('xhr2');

const port = process.env.PORT || 4242;
const host = process.env.HOST || 'localhost';
const server = new Hapi.Server({});
server.connection({
  port,
  host
});

server.register(plugins, err =>
    server.start(err => {
      console.log(`server started at ${server.info.uri}`)
    }));