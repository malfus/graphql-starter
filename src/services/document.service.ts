import * as elasticsearch from 'elasticsearch'

let _client = new elasticsearch.Client({
    host: 'http://username:password@localhost:9200'
})

export function queryAll(index: string) {
    return _client.search({
        index,
        q: '*'
    })
}