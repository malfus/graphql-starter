import * as Good from "good";
import {graphiqlHapi, graphqlHapi} from "graphql-server-hapi";
import {context} from "./context";
import {executableSchema} from "./executableSchema";
import good_options from './config/good';

export const plugins = [
  {
    register: graphqlHapi,
    options: {
      path: '/graphql',
      graphqlOptions: (request) => {
        context.request = request;
        return {
          pretty: true,
          schema: executableSchema,
          context
        }
      },
    },
  },
  {
    register: graphiqlHapi,
    options: {
      path: '/graphiql',
      graphiqlOptions: {
        endpointURL: '/graphql',
      },
    },
  },
  {
    register: Good,
    options: good_options
  }
];