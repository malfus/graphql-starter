/**
 * Created by James on 9/9/2017.
 */
import {Observable} from 'rxjs';
let fetch = require('node-fetch');
let HttpsProxyAgent = require('https-proxy-agent');

export const resolver = {
  Query: {
    randomUsername: () => fetch('https://randomuser.me/api/', { agent:new HttpsProxyAgent({host: '131.84.11.215', port:'9119', rejectUnauthorized:false})})
		.then((res: any) => res.json())
        .then((res: any) => res.results[0].login.username)
  }
};