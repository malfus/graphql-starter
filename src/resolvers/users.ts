import {Observable} from 'rxjs';
let fetch = require('node-fetch');
let HttpsProxyAgent = require('https-proxy-agent');

let fullNameFromName = ({first, last}) => `${first} ${last}`

export const resolver = {
  Query: {
    randomUser: () => fetch('https://randomuser.me/api/', { agent:new HttpsProxyAgent({host: '131.84.11.215', port:'9119', rejectUnauthorized:false})})
		.then((res: any) => res.json())
        .then((res: any) => res.results[0])
  },
  User: {
	friends: (obj, args) => args.num === 0 ? [] : fetch(`https://randomuser.me/api/?results=${args.num || 3}`, { agent:new HttpsProxyAgent({host: '131.84.11.215', port:'9119', rejectUnauthorized:false})})
		.then((res: any) => res.json())
        .then((res: any) => res.results),
	fullName: ({name}) => fullNameFromName(name)
  },
  Name: {
	fullName: fullNameFromName
  }
};

