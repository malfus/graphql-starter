import { queryAll } from "../services/document.service";


export const resolver = {
    Query: {
        search: (obj, args) => queryAll(args.documentType || 'graphtm')
            .then(res => { console.dir(res); return res.hits.hits; })
            .then(hits => hits.map(hit =>
                ({
                    ...hit._source,
                    _type: hit._type
                })
            ))
    },
    GraphTMDocument: {
        title: (obj: GraphTMDocument) => obj.project_title,
        distributionCode: (obj: GraphTMDocument) => obj.distribution_code,
        createdDate: (obj: GraphTMDocument) => obj.create_date,
        modifiedDate: (obj: GraphTMDocument) => obj.modify_date
    },
    Document: {
        __resolveType: ({ _type }) => _type === 'grant' ? 'GraphTMDocument' : 'unknown'
    }
}

export interface GraphTMDocument {
    award_number: string
    proposal_num: string
    project_title: string
    project_narrative: string
    award_abstract: string
    technical_abstract: string
    classification_code: string
    distribution_code: string
    distribution_statement: string
    program_type: string
    funding_division: string
    funding_opportunity: string
    program_announcement: string
    budget: string
    resources: string
    supplement: string
    references_cited: string
    support: string
    bios: string
    program_element: string
    recipient_org_name: string
    recipient_org: number
    poc_last_name: string
    poc_first_name: string
    poc_full_name: string
    poc_type_name: string
    fiscal_year: number
    import_source: string
    start_date: string
    end_date: string
    dod_awarding_office: string
    funding_agency_name: string
    funding_agency_short_name: string
    funding_directorate_name: string
    program_manager: string
    award_amount: number
    create_date: string
    modify_date: string
    award_status: string
    grant_id: number
}