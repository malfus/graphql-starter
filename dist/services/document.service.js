"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var elasticsearch = require("elasticsearch");
var _client = new elasticsearch.Client({
    host: 'http://queryuser:queryuser@localhost:9200'
});
function queryAll(index) {
    return _client.search({
        index: index,
        q: '*'
    });
}
exports.queryAll = queryAll;