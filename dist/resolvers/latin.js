"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var fetch = require('node-fetch');
var HttpsProxyAgent = require('https-proxy-agent');
exports.resolver = {
    Query: {
        randomUsername: function randomUsername() {
            return fetch('https://randomuser.me/api/', { agent: new HttpsProxyAgent({ host: '131.84.11.215', port: '9119', rejectUnauthorized: false }) }).then(function (res) {
                return res.json();
            }).then(function (res) {
                return res.results[0].login.username;
            });
        }
    }
};