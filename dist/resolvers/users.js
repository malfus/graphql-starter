"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var fetch = require('node-fetch');
var HttpsProxyAgent = require('https-proxy-agent');
var fullNameFromName = function fullNameFromName(_ref) {
    var first = _ref.first,
        last = _ref.last;
    return first + " " + last;
};
exports.resolver = {
    Query: {
        randomUser: function randomUser() {
            return fetch('https://randomuser.me/api/', { agent: new HttpsProxyAgent({ host: '131.84.11.215', port: '9119', rejectUnauthorized: false }) }).then(function (res) {
                return res.json();
            }).then(function (res) {
                return res.results[0];
            });
        }
    },
    User: {
        friends: function friends(obj, args) {
            return args.num === 0 ? [] : fetch("https://randomuser.me/api/?results=" + (args.num || 3), { agent: new HttpsProxyAgent({ host: '131.84.11.215', port: '9119', rejectUnauthorized: false }) }).then(function (res) {
                return res.json();
            }).then(function (res) {
                return res.results;
            });
        },
        fullName: function fullName(_ref2) {
            var name = _ref2.name;
            return fullNameFromName(name);
        }
    },
    Name: {
        fullName: fullNameFromName
    }
};