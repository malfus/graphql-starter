"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var document_service_1 = require("../services/document.service");
exports.resolver = {
    Query: {
        search: function search(obj, args) {
            return document_service_1.queryAll(args.documentType || 'graphtm').then(function (res) {
                console.dir(res);return res.hits.hits;
            }).then(function (hits) {
                return hits.map(function (hit) {
                    return Object.assign({}, hit._source, { _type: hit._type });
                });
            });
        }
    },
    GraphTMDocument: {
        title: function title(obj) {
            return obj.project_title;
        },
        distributionCode: function distributionCode(obj) {
            return obj.distribution_code;
        },
        createdDate: function createdDate(obj) {
            return obj.create_date;
        },
        modifiedDate: function modifiedDate(obj) {
            return obj.modify_date;
        }
    },
    Document: {
        __resolveType: function __resolveType(_ref) {
            var _type = _ref._type;
            return _type === 'grant' ? 'GraphTMDocument' : 'unknown';
        }
    }
};