"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Good = require("good");
var graphql_server_hapi_1 = require("graphql-server-hapi");
var context_1 = require("../context");
var executableSchema_1 = require("../executableSchema");
var good_1 = require("./good");
exports.plugins = [{
    register: graphql_server_hapi_1.graphqlHapi,
    options: {
        path: '/graphql',
        graphqlOptions: function graphqlOptions(request) {
            context_1.context.request = request;
            return {
                pretty: true,
                schema: executableSchema_1.executableSchema,
                context: context_1.context
            };
        }
    }
}, {
    register: graphql_server_hapi_1.graphiqlHapi,
    options: {
        path: '/graphiql',
        graphiqlOptions: {
            endpointURL: '/graphql'
        }
    }
}, {
    register: Good,
    options: good_1.default
}];