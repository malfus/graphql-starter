"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var Hapi = require("hapi");
var plugins_1 = require("./plugins");
global.XMLHttpRequest = require('xhr2');
var port = process.env.PORT || 4242;
var host = process.env.HOST || 'localhost';
var server = new Hapi.Server({});
server.connection({
    port: port,
    host: host
});
server.register(plugins_1.plugins, function (err) {
    return server.start(function (err) {
        console.log("server started at " + server.info.uri);
    });
});