"use strict";

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

Object.defineProperty(exports, "__esModule", { value: true });
var graphql_tools_1 = require("graphql-tools");
var path = require("path");
var deepmerge = require("deepmerge");
var schema_1 = require("./schema");
var merge_graphql_schemas_1 = require("merge-graphql-schemas");
var resolversArray = merge_graphql_schemas_1.fileLoader(path.join(__dirname, './resolvers'));
exports.resolvers = deepmerge.all([{}].concat(_toConsumableArray(resolversArray))).resolver;
exports.executableSchema = graphql_tools_1.makeExecutableSchema({
    typeDefs: [schema_1.default],
    resolvers: exports.resolvers
});