Add or edit schemas in the schema directory.  Use the [GraphQL syntax](http://graphql.org/learn/schema/)

Create corresponding resolvers in the resolvers directory.  Resolvers must be of the form: 
```javascript
export const resolver = {
    Query: {
        ...
    },
    Mutation: {
        ...
    },
    Subscription: {
        ...
    }
}
```

To start the server use `npm start`.  Use `npm run build` if you make changes to src.

Access graphiql from `localhost:4242/graphiql`.